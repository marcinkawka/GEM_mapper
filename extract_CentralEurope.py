from netCDF4 import Dataset
import netCDF4 as nc
import numpy as np
import fiona
import numpy.ma as ma
import subprocess
import os
'''
Skrypt wycinajacy obszar naszych zainteresowan (europa srodkowa z pliku NC)
'''


np.set_printoptions(formatter={'float': '{: 0.2f}'.format})

NO_DATA=-999.99



indir='/home/jacek/disk2/diag/gv10/diag_a04/nc/'
outdir='/mnt/array0/mkawka/wymiana/GEM_results/styczen/diag_a06/central_europe/'
#Central Europe mask
maska='/home/mkawka/wymiana/countries/zasieg.shp'


def main(**kwargs):
	cmd ='mkdir -p '+outdir
	subprocess.call(cmd, shell=True)
	cmd ='mkdir -p '+outdir+'tif/'
	subprocess.call(cmd, shell=True)
	
	for file in os.listdir(indir):
		if file.endswith(".nc"):
			name = file[:-3]
			
			infile=indir+name+'.nc'
			outfile=outdir+name+'.nc'
			crop_nc_file(infile,outfile)
			convertToTiff(outfile,name)

def find_nearest_idx(array, value):
	'''Funkcja znajduje najbliższy indeks'''
	array = np.asarray(array)
	idx = (np.abs(array - value)).argmin()
	return idx

def print_keys(d):
	'''Funkcja wyświetla pomocnicze klucze'''
	for k in d.keys():
		print(k,end=" ")
	print("")

def convertToTiff(ncfile,filename):
	
	outfile=outdir+'tif/'+filename
	
	cmd = 'gdal_translate -sds -of GTiff ' + ncfile + " " +outfile+'.tif'
	subprocess.call(cmd, shell=True)
	cmd ='gdalinfo '+ncfile+' |grep NAME '
	
	result=subprocess.check_output(cmd,shell=True)
	res=result.decode('UTF-8')
	i=1
	for row in res.split('\n'):
		band=row.split(':')[-1]
		if band != '':
			cmd='mv '+outfile+'_'+str(i)+'.tif '+outfile+"_"+band+'.tif '
			i=i+1
			subprocess.call(cmd, shell=True)


def crop_nc_file(infile,outfile):
	
	src=Dataset(infile,"r",format="NETCDF3_CLASSIC")
	vars1=src.variables
	dims1=src.dimensions


	shape=fiona.open(maska)
	#pierwszy obiekt z pliku shape
	first=next(iter(shape))

	wierzcholki = first['geometry']['coordinates'][0]
	maskax=[]
	maskay=[]

	lon=np.array(vars1['lon'])
	lat=np.array(vars1['lat'])

	for i in range(len(wierzcholki)):
		lon0=find_nearest_idx(lon,wierzcholki[i][0])
		lat0=find_nearest_idx(lat,wierzcholki[i][1])
		maskax.append(lon0)
		maskay.append(lat0)


	#Pierwsze 5 wymiarów to zmienne pomocnicze (ich nie maskujemy)
	zm=list(vars1.keys())
	print(zm)
	#docelowo trzeba sprawdzić czy istnieją i ewentualnie wyciąć 
	# ['lon', 'lat', 'level', 'level0']
	del zm[0:5]   

	print('Zostaną przycięte następujące zmienne: ')
	print(zm)


	#tworzenie maski o wymiarach takich jak pierwsza zmienna
	#taka działa dla CAMSA
	#MASKA = np.zeros_like(np.array(vars1[zm[0]][0]))
	#a taka dla GEMa
	MASKA = np.zeros_like(np.array(vars1[zm[0]][0][0]))
	print(vars1[zm[0]][0].shape)

	#tam, gdzie wewnątrz szejpa, stawiamy 1
	for i in range(MASKA.shape[0]):
		if(i>min(maskay) and i<max(maskay)):
			for j in range(MASKA.shape[1]):
				if(j>min(maskax) and j<max(maskax)):
					MASKA[i,j]=1

	#Antymaska ustawia "NoData" na zewnątrz maskowanego obszaru
	antyMASKA=(np.ones_like(MASKA)-MASKA)*(NO_DATA)


	print('Utworzona maska o wymiarach')
	print(MASKA.shape)

	#Tworzenie wynikowego pliku
	trg = nc.Dataset(outfile, mode='w',format="NETCDF3_CLASSIC")

	#Create dimensions
	for name, dim in src.dimensions.items():
		trg.createDimension(name, len(dim) if not dim.isunlimited() else None)
	# Copy the global attributes
	trg.setncatts({a:src.getncattr(a) for a in src.ncattrs()})        

	#### Create the variables in the file
	for name, var in src.variables.items():
		trg.createVariable(name, var.dtype, var.dimensions)

	# Copy the variable attributes
		trg.variables[name].setncatts({a:var.getncattr(a) for a in var.ncattrs()})

		# Copy the variables values
		if name in zm:
			#Mnożymy przez zero obszar zewnętrzny
			tmp=np.array(src.variables[name][:])*MASKA
			#i ustawiamy NoDATA na zewnątrz
			tmp=tmp+antyMASKA

			trg.variables[name][:] = ma.masked_array(tmp)
		
		else:
			trg.variables[name][:] = src.variables[name][:]

	trg.close()

if __name__=='__main__':
	main()
	
from qgis.core import  *
from qgis.utils import iface
import pprint
from PyQt5 import QtWidgets
from PyQt5.QtGui import QColor


colDic = {'cziel':'#4bae5a',\
'jziel':'#66bc55',
'zoltawy':'#dede63',
'zolty':'#ffce08',
'czerwony':'#fa4b3c',
'bordowy':'#980c02'}


def generatePDF(iface,output_folder,parametr,legenda_txt):
    pp = pprint.PrettyPrinter(indent=4)
    #generating pdf starts here

    projectInstance= QgsProject.instance()
    projectLayoutManager = projectInstance.layoutManager()
    [pp.pprint(lyr.name()) for lyr in iface.mapCanvas().layers()]
    
        
    for printLayout in projectLayoutManager.printLayouts():
        x, y = 0, 0
        legend = QgsLayoutItemLegend(printLayout)
        root = QgsLayerTree()
        for lyr in (iface.mapCanvas().layers()):
                if (lyr.name() == 'granice Polski'):
                    pass
                elif(lyr.name().find("urban")>-1):
                    pass
                elif(lyr.name().find("rural")>-1):
                    pass
                else:
                    root.addLayer(lyr)
                    
        legend.model().setRootGroup(root)
        printLayout.addItem(legend)    
        #setting new location
        pos=legend.positionWithUnits()
        pos.setX(180)
        pos.setY(20)
        legend.attemptMove(pos)
        
        
        for item in printLayout.items():
            if(isinstance(item, QgsLayoutItemLegend)):
                item.setTitle(legenda_txt)   
                ls=item.legendSettings()
                '''
                #pp.pprint(dir(ls))
                print(ls.boxSpace())
                print(ls.columnCount())
                print(ls.columnSpace())
                print(ls.dpi())
                print(ls.drawRasterStroke())
                '''
                settings = iface.mapCanvas().mapSettings()
                
                
                
                break
        
        inst = QtWidgets.QApplication.instance()
        qapp = QtWidgets.qApp
        #qapp.processEvents()            
        
        export = QgsLayoutExporter(printLayout)
        sett = QgsLayoutExporter.PdfExportSettings()
        res = export.exportToPdf(output_folder+parametr+'.pdf',sett)
        print(res)
import sys
from qgis.core import QgsVectorLayer
from PyQt5.QtCore import QFileInfo,QTimer

from PyQt5 import QtWidgets
inst = QtWidgets.QApplication.instance()
qapp = QtWidgets.qApp

if "VectorLayers" in sys.modules:
    del sys.modules["VectorLayers"]

if "OutputGenerators" in sys.modules:
    del sys.modules["OutputGenerators"]


import pprint
pp = pprint.PrettyPrinter(indent=4)

DEBUGMODE=True

if '/home/marcin/python/' not in sys.path:
    sys.path.append('/home/marcin/python/')



extension='.pdf'

project_path='/home/granit_nfs/GEM_wyniki.qgs'



data_folder='/home/granit_nfs/GEM_results/grudzien/lam_pl_v2_full/poland/2180/'
output_folder='/home/granit_nfs/GEM_results/grudzien/lam_pl_v2_full/poland/pdf/'

#data_folder='/home/granit_nfs/GEM_results/grudzien/lam_pl_v2/poland/2180/'
#output_folder='/home/granit_nfs/GEM_results/grudzien/lam_pl_v2/poland/pdf/'

prefiks = 'diag_lam_pl_2017_v2_'

legenda_txt='diag_lam_pl_2017_v2_full'

if ('vL' in locals()): 
    del vL
    


###################################################################
#param='O3'
#vL=[100,120,150,170,185]
#parametr= prefiks+'O3_lat_long_full_M8HK'
#opis_txt='O3 Max 8H Średnia roczna krocząca'
#agregat='26 maks (S'

#vL=[2000,8000,10000,14000,18000,30000]
#parametr= prefiks+'O3_lat_long_full_AT40'
#parametr= prefiks+'O3_lat_long_AT40'
#opis_txt='O3 - AOT40 ug/m3h'
#agregat='AOT40 V-VI'

#vL=[30,40,50,60,90]
#opis_txt='O3 - Średnia roczna [ug/m3]'
#parametr= prefiks+'O3_lat_long_full_AVYR'
#agregat='Średnia'

#vL=[1,5,15,25,30]
#opis_txt='O3 - Liczba dni z przekroczeniem 120 ug/m3 z 8hk'
#parametr= prefiks+'O3_lat_long_full_D120'
#agregat='L. dni > 1'

#vL=[0,10,20,30,32]
#parametr= prefiks+'O3_lat_long_full_H180'
#opis_txt='O3 - Liczba dni z przekroczeniem 180 ug/m3'
#agregat='L.dni  > 1'

#parametr= prefiks+'O3_lat_long_full_H240'
#opis_txt='O3 - Liczba dni z przekroczeniem 240 ug/m3'
#agregat='L.dni  > 2'

#vL=[80,100,110,120,130]
#parametr= prefiks+'O3_lat_long_full_P932'
#opis_txt='O3 - Percentyl 93.2 [ug/m3]'
#agregat='Per. S93.2'

################################################################### 
#param='SO2'

#vL=[5,10,15,20,100]
#parametr= prefiks+'SO2_lat_long_full_AVYR'
#opis_txt='SO2 - Średnia roczna [ug/m3]'
#agregat='Średnia'

#vL=[5,10,20,30,700]
#parametr= prefiks+'SO2_lat_long_full_AWIN'
#opis_txt='SO2 - Średnia zimowa [ug/m3]'
#agregat='Śr. zimow'

#vL=[1,3,10,15,150]
#parametr= prefiks+'SO2_lat_long_full_D125'
#opis_txt='SO2 - Liczba dni z przekroczeniem 125 ug/m3'
#agregat='L>125 (S24'

#vL=[1,2,3,24,250]
#parametr= prefiks+'SO2_lat_long_full_H350'
#opis_txt='SO2 - Liczba godzin z przekroczeniem 350 ug/m3'
#agregat='L>350 (S1)'

#vL=[20,50,100,150,400,1000]
#parametr= prefiks+'SO2_lat_long_full_P992'
#opis_txt='SO2 - Percentyl 99.2 z rocznej serii średnich 24h'
#agregat='Perc. 99.2'

#vL=[20,50,100,150,400,1000]
#parametr= prefiks+'SO2_lat_long_full_P997'
#opis_txt='SO2 - Percentyl 99.7 z rocznej serii średnich 1h'
#agregat='Perc. 99.7'
################################################################### 
param='PM10'
#vL=[15,20,30,40,150]
#parametr= prefiks+'PM10_lat_long_full_AVYR'
#opis_txt='PM10 - Średnia roczna [ug/m3]'
#agregat='Średnia'

#vL=[15,35,60,90,150,450]
#parametr= prefiks+'PM10_lat_long_full_D050'
#opis_txt='PM10 - Liczba dni z przekroczeniem 50ug/m3'
#agregat='L>50 (S24)'

#vL=[30,40,60,90,120,450]
#parametr= prefiks+'PM10_lat_long_full_P904'
#opis_txt='PM10 - Percentyl 90.4 z rocznej serii średnich 24h'
#agregat='Perc. 90.4'

vL=[30,40,50,70,100,450]
parametr= prefiks+'PM10_lat_long_full_MX36'
opis_txt='PM10 - 36 maksimum [ug/m3]'
agregat='36 maks. ('

################################################################### 
#param='PM25'
#vL=[10,15,20,25,40,50]
#parametr= prefiks+'PM25_lat_long_full_AVYR'
#opis_txt='PM25 - Średnia roczna [ug/m3]'
#agregat='Średnia'

################################################################### 
#param='NO2'
#vL=[10,15,20,40,60]
#parametr= prefiks+'NO2_lat_long_full_AVYR'
#opis_txt='NO2 - Średnia roczna [ug/m3]'
#agregat='Średnia'

#vL=[1,2,3,18,70]
#parametr= prefiks+'NO2_lat_long_full_H200'
#opis_txt='NO2 - Liczba godzin z przekroczeniem 200 ug/m3'
#agregat='L>200 (S1)'

#vL=[30,60,100,150,250]
#parametr= prefiks+'NO2_lat_long_full_MX1H'
#opis_txt='NO2 -Maksimum 1h [ug/m3] '
#agregat='Maks'

#vL=[40,60,90,120,250]
#parametr= prefiks+'NO2_lat_long_full_P998'
#opis_txt='NO2 - Percentyl 99.8 z rocznej serii 1h [ug/m3] '
#agregat='Perc. 99.8'
##############################################3
#param='NOX'
#vL=[10,15,20,30,60,250]
#parametr= prefiks+'NOX_lat_long_full_AVYR'
#opis_txt='NOX - Średnia roczna [ug/m3]'
#agregat='Średnia'

#tworzenie projektu
iface.newProject()

projectInstance= QgsProject.instance()

# loading existing project
projectInstance.read(project_path)


  
rlayer = QgsRasterLayer(data_folder+parametr+'.tif',opis_txt)

if not rlayer.isValid():
  print("Layer failed to load!")
  

#zmiana min/max
renderer = rlayer.renderer()
provider = rlayer.dataProvider()
extent = rlayer.extent()

ver = provider.hasStatistics(1, QgsRasterBandStats.All)
stats = provider.bandStatistics(1, QgsRasterBandStats.All,extent, 0)

min = stats.minimumValue
max = stats.maximumValue
range = max - min
interval = range/4

if not ('vL' in locals()):
    vL =[min, min+ interval,min+ 2*interval,min+ 3*interval, max]
#paleta zielony-żółty-czerwony

colDic = {'cziel':'#4bae5a',\
'jziel':'#88c95a',
'zoltawy':'#dede63',
'zolty':'#ffce08',
'czerwony':'#fa4b3c',
'bordowy':'#980c02'}
if(len(vL)<6):
    lst = [ QgsColorRampShader.ColorRampItem(vL[0], QColor(colDic['cziel']),'< '+format(vL[0], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[1], QColor(colDic['jziel']),format(vL[0], '.0f')+' - '+format(vL[1], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[2], QColor(colDic['zoltawy']),format(vL[1], '.0f')+' - '+format(vL[2], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[3], QColor(colDic['zolty']),format(vL[2], '.0f')+' - '+format(vL[3], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[4], QColor(colDic['czerwony']),'> '+format(vL[3], '.0f'))]
else:
    lst = [ QgsColorRampShader.ColorRampItem(vL[0], QColor(colDic['cziel']),'< '+format(vL[0], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[1], QColor(colDic['jziel']),format(vL[0], '.0f')+' - '+format(vL[1], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[2], QColor(colDic['zoltawy']),format(vL[1], '.0f')+' - '+format(vL[2], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[3], QColor(colDic['zolty']),format(vL[2], '.0f')+' - '+format(vL[3], '.0f')), 
            QgsColorRampShader.ColorRampItem(vL[4], QColor(colDic['czerwony']),format(vL[3], '.0f')+' - '+format(vL[4], '.0f')),
            QgsColorRampShader.ColorRampItem(vL[5], QColor(colDic['bordowy']),'> '+format(vL[4], '.0f'))]
myRasterShader = QgsRasterShader()
myColorRamp = QgsColorRampShader()

myColorRamp.setColorRampItemList(lst)
myColorRamp.setColorRampType(QgsColorRampShader.Discrete)
myRasterShader.setRasterShaderFunction(myColorRamp)

myPseudoRenderer = QgsSingleBandPseudoColorRenderer(rlayer.dataProvider(), 
                                                    rlayer.type(),  
                                                    myRasterShader)

rlayer.setRenderer(myPseudoRenderer)
rlayer.triggerRepaint()                                                    

if not rlayer.isValid():
    print("Failed to load layer")

#resampleFilter = rlayer.resampleFilter()
#resampleFilter.setZoomedInResampler(QgsBilinearRasterResampler())
#resampleFilter.setZoomedOutResampler(QgsBilinearRasterResampler())

projectInstance.addMapLayer(rlayer)


    
from VectorLayers import *
load_borders()
load_urban(param,agregat,vL)
load_suburban(param,agregat,vL)
load_rural(param,agregat,vL)

qapp.processEvents()


from OutputGenerators import generatePDF
generatePDF(iface,output_folder,parametr,legenda_txt)
    
    
    
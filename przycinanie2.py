from netCDF4 import Dataset
import netCDF4 as nc
import numpy as np
import fiona
import numpy.ma as ma
'''
Skrypt wycinajacy obszar naszych zainteresowan (europa srodkowa z pliku NC)
'''


np.set_printoptions(formatter={'float': '{: 0.2f}'.format})

#file names from CAMS
#ENSa.2017.NO2.avg.nc  ENSa.2017.O3.aot40.nc  ENSa.2017.O3.t120.nc  ENSa.2017.PM10.avg.nc  ENSa.2017.PM25.avg.nc

#file names from Jacek
#diag_2017_NO2_lat_long.nc  diag_2017_NOX_lat_long.nc  diag_2017_O3_lat_long.nc  diag_2017_PM10_lat_long.nc  diag_2017_PM25_lat_long.nc  diag_2017_SO2_lat_long.nc

#CAMS
#NO_DATA=-999

#GEM
NO_DATA=-999.99

Jacek_files=['diag_2017_NO2_lat_long','diag_2017_NOX_lat_long','diag_2017_O3_lat_long','diag_2017_PM10_lat_long','diag_2017_PM25_lat_long','diag_2017_SO2_lat_long']


base_dir='/home/mkawka/wymiana/GEM_poniedzialek/'

maska='/home/mkawka/wymiana/countries/zasieg.shp'


def main(**kwargs):
	for plik in Jacek_files:
		output='/home/mkawka/wymiana/GEM_poniedzialek/nc/croped_'+plik+'.nc'
		crop_nc_file(plik,output)
	
def find_nearest_idx(array, value):
	'''Funkcja znajduje najbliższy indeks'''
	array = np.asarray(array)
	idx = (np.abs(array - value)).argmin()
	return idx

def print_keys(d):
	'''Funkcja wyświetla pomocnicze klucze'''
	for k in d.keys():
		print(k,end=" ")
	print("")

def crop_nc_file(fileName,output):
	
	inputt=base_dir+fileName+'.nc'
	src=Dataset(inputt,"a",format="NETCDF3_CLASSIC")
	vars1=src.variables
	dims1=src.dimensions


	shape=fiona.open(maska)
	#pierwszy obiekt z pliku shape
	first=next(iter(shape))

	wierzcholki = first['geometry']['coordinates'][0]
	maskax=[]
	maskay=[]

	lon=np.array(vars1['lon'])
	lat=np.array(vars1['lat'])

	for i in range(len(wierzcholki)):
		lon0=find_nearest_idx(lon,wierzcholki[i][0])
		lat0=find_nearest_idx(lat,wierzcholki[i][1])
		maskax.append(lon0)
		maskay.append(lat0)


	#Pierwsze 5 wymiarów to zmienne pomocnicze (ich nie maskujemy)
	zm=list(vars1.keys())
	print(zm)
	#docelowo trzeba sprawdzić czy istnieją i ewentualnie wyciąć 
	# ['lon', 'lat', 'level', 'level0']
	del zm[0:5]   

	print('Zostaną przycięte następujące zmienne: ')
	print(zm)


	#tworzenie maski o wymiarach takich jak pierwsza zmienna
	#taka działa dla CAMSA
	#MASKA = np.zeros_like(np.array(vars1[zm[0]][0]))
	#a taka dla GEMa
	MASKA = np.zeros_like(np.array(vars1[zm[0]][0][0]))
	print(vars1[zm[0]][0].shape)

	#tam, gdzie wewnątrz szejpa, stawiamy 1
	for i in range(MASKA.shape[0]):
		if(i>min(maskay) and i<max(maskay)):
			for j in range(MASKA.shape[1]):
				if(j>min(maskax) and j<max(maskax)):
					MASKA[i,j]=1

	#Antymaska ustawia "NoData" na zewnątrz maskowanego obszaru
	antyMASKA=(np.ones_like(MASKA)-MASKA)*(NO_DATA)


	print('Utworzona maska o wymiarach')
	print(MASKA.shape)

	#Tworzenie wynikowego pliku
	trg = nc.Dataset(output, mode='w',format="NETCDF3_CLASSIC")

	#Create dimensions
	for name, dim in src.dimensions.items():
		trg.createDimension(name, len(dim) if not dim.isunlimited() else None)
	# Copy the global attributes
	trg.setncatts({a:src.getncattr(a) for a in src.ncattrs()})        

	#### Create the variables in the file
	for name, var in src.variables.items():
		trg.createVariable(name, var.dtype, var.dimensions)

	# Copy the variable attributes
		trg.variables[name].setncatts({a:var.getncattr(a) for a in var.ncattrs()})

		# Copy the variables values
		if name in zm:
			#Mnożymy przez zero obszar zewnętrzny
			tmp=np.array(src.variables[name][:])*MASKA
			#i ustawiamy NoDATA na zewnątrz
			tmp=tmp+antyMASKA

			trg.variables[name][:] = ma.masked_array(tmp)
		
		else:
			trg.variables[name][:] = src.variables[name][:]

	trg.close()

if __name__=='__main__':
	main()
	
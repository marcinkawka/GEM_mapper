import subprocess
import os
import fiona
import rasterio
import rasterio.mask
 
inshape = '/home/mkawka/wymiana/countries/poland.shp' 
#indir ='/mnt/nas/jacek/diag/gv10/12m-grids-nox/diag-nc/'
indir ='/mnt/nas/jacek/diag/lam/v2_partial/diag-nc/'
outdir ='/mnt/array0/mkawka/wymiana/GEM_results/grudzien/lam_pl_v2a/'


def convertToNetCDF(filename):
	infile=indir+filename+'.nc'
	outfile=outdir+filename
	# make gdal_rasterize command - will burn value 0 to raster where polygon intersects 
	cmd = 'gdal_translate -sds -of GTiff ' + infile + " " +outfile+'.tif'
	#print(cmd)
	# run command
	subprocess.call(cmd, shell=True)
	cmd ='gdalinfo '+infile+' |grep NAME '
	#print(cmd)
	result=subprocess.check_output(cmd,shell=True)
	res=result.decode('UTF-8')
	i=1
	for row in res.split('\n'):
		band=row.split(':')[-1]
		if band != '':
			cmd='mv '+outfile+'_'+str(i)+'.tif '+outfile+"_"+band+'.tif '
			i=i+1
			subprocess.call(cmd, shell=True)


def extractPoland(indir):
	outdir=indir+'poland/'
	with fiona.open(inshape, "r") as shapefile:
		features = [feature["geometry"] for feature in shapefile]

	for file in os.listdir(indir):
		if file.endswith(".tif"):
			inraster = os.path.join(indir, file)
			outraster= os.path.join(outdir, file)
			with rasterio.open(inraster) as src:
				out_image, out_transform = rasterio.mask.mask(src, features,crop=True)
				out_meta = src.meta.copy()



				out_meta.update({"driver": "GTiff",
					 "height": out_image.shape[1],
					 "width": out_image.shape[2],
					 "transform": out_transform})

				with rasterio.open(outraster, "w", **out_meta) as dest:
					dest.write(out_image.astype(rasterio.float32))

def convertCoordinates(indir):
	for file in os.listdir(indir):
		if file.endswith(".tif"):
			inraster = os.path.join(indir, file)
			outraster = os.path.join(indir+'2180/', file)
			
			cmd = 'gdalwarp -r bilinear -s_srs EPSG:4326 -t_srs EPSG:2180 -overwrite '+ inraster  +' '+outraster 
			
			#print(cmd)
			# run command
			subprocess.call(cmd, shell=True)


def main(**kwargs):
	cmd ='mkdir -p '+outdir
	subprocess.call(cmd, shell=True)

	for file in os.listdir(indir):
		if file.endswith(".nc"):
			name = file[:-3]
			convertToNetCDF(name)

	cmd ='mkdir -p '+outdir+'poland/'
	subprocess.call(cmd, shell=True)

				
	extractPoland(outdir)

	cmd ='mkdir -p '+outdir+'poland/2180/'
	subprocess.call(cmd, shell=True)

	convertCoordinates(outdir+'poland/')

if __name__=='__main__':

	main()
		
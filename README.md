A set of batch processing scripts for generating maps for reports, based on GEM-AQ results in NetCDF

## Typical processing order is:

1. **nc_translate.py** - converts netCDF files to tiffs
2. **przycinanie_rastrem.py** - cuts Poland (or Central Europe) from tiff files
3. **zmiana_ukladu.py** - converts coordinate system (from WGS 1984 to Poland 1992)
4. **generateMapLayout.py** - generates pdf with map (QGIS 3.4 required)
 

from qgis.core import  *
from PyQt5.QtGui import QColor

DEBUGMODE=True

shp_src = '/home/granit_nfs/GEM_countries/2180/granice_polski.shp'

#style dla stacji
urbanStyle='/home/marcin/granit_nfs/csv/urbanStyle.qml'
stylGranic='/home/granit_nfs/stylGranic.qml'

colDic = {'cziel':'#4bae5a',\
'jziel':'#88c95a',
'zoltawy':'#dede63',
'zolty':'#ffce08',
'czerwony':'#fa4b3c',
'bordowy':'#980c02'}

def setColors(vL):
    if(len(vL)<6):
        colors= (
            (' <'+format(vL[0],'.0f'),0.0,vL[0],'cziel'),
            (format(vL[0],'.0f')+' - '+format(vL[1],'.0f'),vL[0],vL[1],'jziel'),
            (format(vL[1],'.0f')+' - '+format(vL[2],'.0f'),vL[1],vL[2],'zoltawy'),
            (format(vL[2],'.0f')+' - '+format(vL[3],'.0f'),vL[2],vL[3],'zolty'),
            (' > '+format(vL[3],'.0f'),vL[3],vL[4],'czerwony')
        )
    else:
        colors= (
            (' <'+format(vL[0],'.0f'),0.0,vL[0],'cziel'),
            (format(vL[0],'.0f')+' - '+format(vL[1],'.0f'),vL[0],vL[1],'jziel'),
            (format(vL[1],'.0f')+' - '+format(vL[2],'.0f'),vL[1],vL[2],'zoltawy'),
            (format(vL[2],'.0f')+' - '+format(vL[3],'.0f'),vL[2],vL[3],'zolty'),
            (format(vL[3],'.0f')+' - '+format(vL[4],'.0f'),vL[3],vL[4],'czerwony'),
            (' > '+format(vL[4],'.0f'),vL[4],vL[5],'bordowy')
        )        
    return colors
    
def load_urban(param,agregat,vL):
    st_src = '/home/granit_nfs/lokalizacje_stacji/'+param+'_2017_urban.shp'
    #print(st_src)
    stacje_urban= QgsVectorLayer(st_src, 'urban '+param+" "+agregat,"ogr")
    stacje_urban.loadNamedStyle(urbanStyle)
    colors=setColors(vL)
    

    ranges=[]
    for label, lower, upper, color in colors:
        symbol = QgsMarkerSymbol.createSimple({'name' : 'square'})
        symbol.setColor(QColor(colDic[color]))
        rng = QgsRendererRange(lower, upper, symbol, label)
        ranges.append(rng)
        
    #expression = 'Średnia' # field name
    renderer = QgsGraduatedSymbolRenderer(agregat, ranges)
    stacje_urban.setRenderer(renderer)
    
    projectInstance= QgsProject.instance()
    projectInstance.addMapLayer(stacje_urban)

def load_rural(param,agregat,vL):
    st_src = '/home/granit_nfs/lokalizacje_stacji/'+param+'_2017_rural.shp'
   # print(st_src)
    stacje_urban= QgsVectorLayer(st_src, 'rural '+param+" "+agregat,"ogr")
    stacje_urban.loadNamedStyle(urbanStyle)
    
    colors=setColors(vL)
    
    ranges=[]
    for label, lower, upper, color in colors:
        symbol = QgsMarkerSymbol.createSimple({'name' : 'circle'})
        symbol.setColor(QColor(colDic[color]))
        rng = QgsRendererRange(lower, upper, symbol, label)
        ranges.append(rng)
        
    #expression = 'Średnia' # field name
    renderer = QgsGraduatedSymbolRenderer(agregat, ranges)
    stacje_urban.setRenderer(renderer)
    
    projectInstance= QgsProject.instance()
    projectInstance.addMapLayer(stacje_urban)

def load_suburban(param,agregat,vL):
    st_src = '/home/granit_nfs/lokalizacje_stacji/'+param+'_2017_suburban.shp'
   # print(st_src)
    stacje_urban= QgsVectorLayer(st_src, 'suburban '+param+" "+agregat,"ogr")
    stacje_urban.loadNamedStyle(urbanStyle)
    
    colors=setColors(vL)
    ranges=[]
    for label, lower, upper, color in colors:
        symbol = QgsMarkerSymbol.createSimple({'name' : 'triangle'})
        symbol.setColor(QColor(colDic[color]))
        rng = QgsRendererRange(lower, upper, symbol, label)
        ranges.append(rng)
        
    #expression = 'Średnia' # field name
    renderer = QgsGraduatedSymbolRenderer(agregat, ranges)
    stacje_urban.setRenderer(renderer)
    
    projectInstance= QgsProject.instance()
    projectInstance.addMapLayer(stacje_urban)    

def load_borders():
    ''' loads borders to default project instance'''
    granice =QgsVectorLayer(shp_src, "granice Polski", "ogr")
    granice.loadNamedStyle(stylGranic)
    if not granice.isValid():
       print("Layer failed to load!")
    projectInstance= QgsProject.instance()
    projectInstance.addMapLayer(granice)
   
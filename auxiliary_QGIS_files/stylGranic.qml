<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+8" labelsEnabled="0" simplifyDrawingTol="1" version="3.4.1-Madeira" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" maxScale="0" styleCategories="AllStyleCategories" simplifyDrawingHints="1" simplifyMaxScale="1" readOnly="0" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" enableorderby="0" forceraster="0" symbollevels="0">
    <symbols>
      <symbol type="line" alpha="1" name="0" clip_to_extent="1">
        <layer class="SimpleLine" locked="0" pass="0" enabled="1">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.26" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeType="MM" labelPlacementMethod="XHeight" backgroundColor="#ffffff" sizeType="MM" diagramOrientation="Up" width="15" minScaleDenominator="0" scaleDependency="Area" opacity="1" maxScaleDenominator="1e+8" penColor="#000000" barWidth="5" enabled="0" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" height="15" penAlpha="255" minimumSize="0" rotationOffset="270" penWidth="0" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="Sans,10,-1,5,50,0,0,0,0,0"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" placement="2" zIndex="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="EU_FLAG">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="EFTA_FLAG">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="CC_FLAG">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="CNTR_BN_ID">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="OTHR_FLAG">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="COAS_FLAG">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="FID">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="EU_FLAG" name=""/>
    <alias index="1" field="EFTA_FLAG" name=""/>
    <alias index="2" field="CC_FLAG" name=""/>
    <alias index="3" field="CNTR_BN_ID" name=""/>
    <alias index="4" field="OTHR_FLAG" name=""/>
    <alias index="5" field="COAS_FLAG" name=""/>
    <alias index="6" field="FID" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="EU_FLAG"/>
    <default expression="" applyOnUpdate="0" field="EFTA_FLAG"/>
    <default expression="" applyOnUpdate="0" field="CC_FLAG"/>
    <default expression="" applyOnUpdate="0" field="CNTR_BN_ID"/>
    <default expression="" applyOnUpdate="0" field="OTHR_FLAG"/>
    <default expression="" applyOnUpdate="0" field="COAS_FLAG"/>
    <default expression="" applyOnUpdate="0" field="FID"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" unique_strength="0" field="EU_FLAG" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="EFTA_FLAG" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="CC_FLAG" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="CNTR_BN_ID" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="OTHR_FLAG" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="COAS_FLAG" constraints="0" exp_strength="0"/>
    <constraint notnull_strength="0" unique_strength="0" field="FID" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="EU_FLAG" exp=""/>
    <constraint desc="" field="EFTA_FLAG" exp=""/>
    <constraint desc="" field="CC_FLAG" exp=""/>
    <constraint desc="" field="CNTR_BN_ID" exp=""/>
    <constraint desc="" field="OTHR_FLAG" exp=""/>
    <constraint desc="" field="COAS_FLAG" exp=""/>
    <constraint desc="" field="FID" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" hidden="0" name="EU_FLAG" width="-1"/>
      <column type="field" hidden="0" name="EFTA_FLAG" width="-1"/>
      <column type="field" hidden="0" name="CC_FLAG" width="-1"/>
      <column type="field" hidden="0" name="CNTR_BN_ID" width="-1"/>
      <column type="field" hidden="0" name="OTHR_FLAG" width="-1"/>
      <column type="field" hidden="0" name="COAS_FLAG" width="-1"/>
      <column type="field" hidden="0" name="FID" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Formularze QGIS mogą zawierać funkcje Pythona, które będą wywołane przy otwieraniu
 formularza.

Można z nich skorzystać, aby rozbudować formularz.

Wpisz nazwę funkcji w polu
"Python Init function".
Przykład:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="CC_FLAG" editable="1"/>
    <field name="CNTR_BN_ID" editable="1"/>
    <field name="COAS_FLAG" editable="1"/>
    <field name="EFTA_FLAG" editable="1"/>
    <field name="EU_FLAG" editable="1"/>
    <field name="FID" editable="1"/>
    <field name="OTHR_FLAG" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="CC_FLAG"/>
    <field labelOnTop="0" name="CNTR_BN_ID"/>
    <field labelOnTop="0" name="COAS_FLAG"/>
    <field labelOnTop="0" name="EFTA_FLAG"/>
    <field labelOnTop="0" name="EU_FLAG"/>
    <field labelOnTop="0" name="FID"/>
    <field labelOnTop="0" name="OTHR_FLAG"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>CNTR_BN_ID</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
